/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   max_len.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:07:12 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:06:09 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"
#include "ft_printf.h"

static void			calculate_max_len(t_data p, t_max_len *m)
{
	INIT(struct passwd *, pwuid, NULL);
	INIT(struct group *, grgid, NULL);
	INIT(t_file_list *, lst, p.d.dir_content);
	while (lst)
	{
		if (lst->file_name[0] != '.' || p.o.f_a)
		{
			pwuid = getpwuid(lst->file_infos->st_uid);
			grgid = getgrgid(lst->file_infos->st_gid);
			if (m->len_max_1 < ft_intlen(lst->file_infos->st_nlink))
				m->len_max_1 = ft_intlen(lst->file_infos->st_nlink);
			if (m->len_max_2 < ft_strlen(pwuid->pw_name))
				m->len_max_2 = ft_strlen(pwuid->pw_name);
			if (m->size_max < ft_intlen(lst->file_infos->st_size))
				m->size_max = ft_intlen(lst->file_infos->st_size);
			if (m->len_max_3 < ft_strlen(grgid->gr_name) + m->size_max)
				m->len_max_3 = ft_strlen(grgid->gr_name) + m->size_max;
			if (m->len_max_4 < ft_intlen(lst->file_infos->st_ino))
				m->len_max_4 = ft_intlen(lst->file_infos->st_ino);
			if (m->len_max_5 < ft_strlen(lst->file_name))
				m->len_max_5 = ft_strlen(lst->file_name);
		}
		lst = lst->next;
	}
}

static t_max_len	reset(void)
{
	t_max_len m;

	m.len_max_1 = 0;
	m.len_max_2 = 0;
	m.len_max_3 = 0;
	m.size_max = 0;
	m.len_max_4 = 0;
	m.len_max_5 = 0;
	return (m);
}

size_t				get_max_len(t_data p, int column)
{
	static t_max_len m = {0, 0, 0, 0, 0, 0};
	
	if (column == 0)
	{
		m = reset();
		return (0);
	}
	if (m.len_max_1 > 0 && column == 1)
		return (m.len_max_1);
	if (m.len_max_2 > 0 && column == 2)
		return (m.len_max_2);
	if (m.len_max_3 > 0 && column == 3)
		return (m.len_max_3);
	if (m.len_max_4 > 0 && column == 4)
		return (m.len_max_4);
	if (m.len_max_5 > 0 && column == 5)
		return (m.len_max_5);
	calculate_max_len(p, &m);
	if (column == 4 || column == 5)
		return (column == 4) ? (m.len_max_4) : (m.len_max_5);
	if (column == 1)
		return (m.len_max_1);
	return (column == 2) ? (m.len_max_2) : (m.len_max_3);
}
