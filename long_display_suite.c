/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_display_suite.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:28:24 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:16:31 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"
#include "ft_printf.h"

void	print_owner(t_data p)
{
	struct passwd 	*infos;

	if (!(infos = getpwuid(p.d.dir_content->file_infos->st_uid)))
	{
		ft_printf("%d", p.d.dir_content->file_infos->st_uid);
		print_space_2(p);
		return ;
	}
	ft_putstr(infos->pw_name);
	print_space_2(p);
}

void	print_group(t_data p)
{
	struct group 	*infos;
	
	if (!(infos = getgrgid(p.d.dir_content->file_infos->st_gid)))
	{
		ft_printf("%d", p.d.dir_content->file_infos->st_gid);
		return ;
	}
	ft_putstr(infos->gr_name);
	print_space_3(p);
}
