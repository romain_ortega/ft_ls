/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 12:59:13 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:05:09 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"

size_t	count_dir_path(t_data *p, char *path)
{
	DIR 			*ret_dir 	= NULL;
	struct dirent 	*file		= NULL;
	size_t 			elem		= 0;
	
	if (!path || !(ret_dir = opendir(path)))
	{
		if (errno != ENOTDIR)
			p->e.error_set = true;
		else
			p->o.f_big_r = false;
		return (0);
	}
	while ((file = readdir(ret_dir)) != NULL)
		if (file->d_type == DT_DIR && (*file->d_name != '.' || p->o.f_a) &&
		ft_strcmp(file->d_name, ".") != 0 && ft_strcmp(file->d_name, "..") != 0)
			elem++;
	if (closedir(ret_dir) == -1)
		exit_perror();
	return (elem);
}
