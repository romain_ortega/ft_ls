/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_colors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:10:40 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:18:41 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"
#include "ft_printf.h"

void	reset_color(void)
{
	ft_putstr("\033[0m");
}

void	print_colors(t_data p)
{
	if (p.d.dir_content->type == 'o')
		return (ft_putstr("\033[30m\033[43m"));
	if (p.d.dir_content->type == 'd'
	&& p.d.dir_content->file_infos->st_mode & S_ISVTX
	&& p.d.dir_content->file_infos->st_mode & 0100
	&& p.d.dir_content->file_infos->st_mode & S_IWOTH)
		ft_putstr("\033[30m\033[42m");
	else if (p.d.dir_content->type == 'l')
		ft_putstr("\033[35m");
	else if (p.d.dir_content->type == 's')
		ft_putstr("\033[32m");
	else if (p.d.dir_content->type == 'c')
		ft_putstr("\033[35\033[43m");
	else if (p.d.dir_content->type == 'b')
		ft_putstr("\033[35\033[44m");
	else if (p.d.dir_content->type == 'd')
		ft_putstr("\033[36m");
	else if (p.d.dir_content->type == 'p')
		ft_putstr("\033[33m");
	else if (p.d.dir_content->type == '-'
	&& p.d.dir_content->file_infos->st_mode & S_IXOTH)
		ft_putstr("\033[31m");
	else
		reset_color();
}

void	print_type_indicator(t_data p)
{
	if ((p.d.dir_content->type == 'd' || p.d.dir_content->type == 'o')
	&& p.o.f_p)
		ft_putchar('/');
	if (!p.o.f_big_f)
		return ;
	if (p.d.dir_content->type == 'l')
		ft_putchar('@');
	else if (p.d.dir_content->type == 'p')
		ft_putchar('|');
	else if (p.d.dir_content->type == '-'
	&& p.d.dir_content->file_infos->st_mode & S_IXOTH)
		ft_putchar('*');
}
