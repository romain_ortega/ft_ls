/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copy_dir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 17:10:08 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:06:45 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"
#include "ft_ls.h"

char		*join_path(char *file, char *path)
{
	char 	*resolved_path 	= NULL;
	char 	*tmp			= NULL;
	char 	*s				= NULL;

	if (!file || !path)
		return (NULL);
	if (!(resolved_path = (char *)malloc(sizeof(char) * PATH_MAX)))
		exit_error("error: malloc fail");
	realpath(path, resolved_path);
	tmp = ft_strnew(ft_strlen(resolved_path) + 2);
	ft_strcpy(tmp, resolved_path);
	ft_strcat(tmp, "/");
	s = ft_strnew(ft_strlen(tmp) + ft_strlen(file) + 1);
	ft_strcpy(s, tmp);
	ft_strcat(s, file);
	ft_strdel(&tmp);
	ft_strdel(&resolved_path);
	return (s);
}

static bool	copy_link_info(t_data *p, struct dirent *file,
			struct stat *file_infos, char *path)
{
	if (!file_infos)
		return (true);
	ft_strcpy(file->d_name, path);
	p->d.nb_dir_elem = 1;
	file->d_type = DT_LNK;
	if (p->d.path->content)
		ft_strdel((char **)&p->d.path->content);
	p->d.path->content = ft_strdup("/link");
	p->d.dir_content = lst_new_file(file, file_infos);
	free(file_infos);
	return (true);
}

static bool	check_link(t_data *p, char *path)
{
	if (!p->o.f_l || ft_strlen(path) < 2 || !path)
		return (false);
		
	DIR 			*ret_dir	= NULL;
	struct dirent 	*file		= NULL;
	struct stat 	*file_infos	= NULL;
	char 			*real_path	= path;
	char 			*dir_name	= ft_strrchr(path, '/') + 1;

	if (ft_strrchr(path, '/') && path[ft_strlen(path) - 1] != '/')
		real_path = ft_strrchr((char const *)ft_strrev(path), '/');
	else
		return (false);
	if (!(ret_dir = opendir(real_path)))
		return (true);
	if (!(file_infos = (struct stat *)malloc(sizeof(struct stat))))
		exit_error("error: malloc fail");
	while ((file = readdir(ret_dir)) != NULL)
		if (file->d_type == DT_LNK && ft_strcmp(file->d_name, dir_name) == 0)
		{
			if ((lstat(path, file_infos)) < 0)
				dup_error(p, path);
			return (copy_link_info(p, file, file_infos, path));
		}
	closedir(ret_dir);
	return (false);
}

static void	read_dir(t_data *p, DIR *ret_dir, char *path, size_t *n)
{
	char 			*tmp		= NULL;
	struct dirent 	*file		= NULL;
	struct stat 	*file_infos	= NULL;

	while ((file = readdir(ret_dir)) != NULL)
	{
		if (!(file_infos = (struct stat *)malloc(sizeof(struct stat))))
			exit_error("error: malloc fail");
		tmp = join_path(file->d_name, path);
		if ((lstat((const char *)tmp, file_infos)) < 0)
			return (dup_error(p, path));
		if (p->d.dir_content == NULL)
		{
			if (!(p->d.dir_content = lst_new_file(file, file_infos)))
				exit_perror();
		}
		else
			lst_push_new_file(&p->d.dir_content, file, file_infos);
		free(file_infos);
		ft_strdel(&tmp);
		*n += 1;
	}
}

void		copy_dir_content(t_data *p, char *path)
{
	DIR 	*ret_dir 	= NULL;
	size_t	n			= 0;

	ret_dir = opendir((!path ? "." : path));
	if (!ret_dir)
	{
		if (ret_dir)
			if (closedir(ret_dir) == -1)
				exit_perror();
		return (dup_error(p, path));
	}
	p->d.dir_content = NULL;
	p->d.nb_dir_elem = 0;
	if (check_link(p, path))
	{
		if (closedir(ret_dir) == -1 && ret_dir)
			exit_perror();
		return ;
	}
	read_dir(p, ret_dir, path, &n);
	p->d.nb_dir_elem = n;
	if (closedir(ret_dir) == -1)
		exit_perror();
}
