/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_files.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:31:02 by rortega           #+#    #+#             */
/*   Updated: 2016/02/01 15:31:04 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"

static char		get_type(struct dirent *file)
{
	if (file->d_type == DT_DIR)
		return ('d');
	else if (file->d_type == DT_LNK)
		return ('l');
	else if (file->d_type == DT_FIFO)
		return ('p');
	else if (file->d_type == DT_SOCK)
		return ('s');
	else if (file->d_type == DT_BLK)
		return ('b');
	else if (file->d_type == DT_CHR)
		return ('c');
	else
		return ('-');
}

t_file_list		*lst_new_file(struct dirent *file, struct stat *file_infos)
{
	t_file_list	*new_list;

	if (!(new_list = (t_file_list*)malloc(sizeof(t_file_list))))
		return (NULL);
	if (!(new_list->file_infos = (struct stat *)malloc(sizeof(struct stat))))
		exit_error("error: malloc fail");
	new_list->file_name = (file != NULL) ? (ft_strdup(file->d_name)) : (NULL);
	if (file_infos != NULL)
		ft_memcpy(new_list->file_infos, file_infos, sizeof(struct stat));
	new_list->type = (file != NULL) ? (get_type(file)) : (0);
	new_list->error = NULL;
	new_list->next = NULL;
	return (new_list);
}

void			lst_push_new_file(t_file_list **alst, struct dirent *file,
				struct stat *file_infos)
{
	t_file_list *elem;

	if (!alst)
		*alst = lst_new_file(file, file_infos);
	else if ((elem = *alst) != NULL)
	{
		if (elem->next)
			return (lst_push_new_file(&(elem->next), file, file_infos));
		elem->next = lst_new_file(file, file_infos);
	}
}

static void		lst_del_file(t_file_list **alst)
{
	if (!alst)
		return ;
	if ((*alst)->file_name)
		free((*alst)->file_name);
	if ((*alst)->error)
		free((*alst)->error);
	(*alst)->file_name = NULL;
	(*alst)->error = NULL;
	if ((*alst)->file_infos)
		free((*alst)->file_infos);
	(*alst)->file_infos = NULL;
	free(*alst);
	*alst = NULL;
}

void			lst_file_del(t_file_list **alst)
{
	t_file_list	*next;

	if (!alst)
		return ;
	while (*alst)
	{
		next = (*alst)->next;
		lst_del_file(alst);
		*alst = next;
	}
}
