# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rortega <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/22 18:15:40 by rortega           #+#    #+#              #
#    Updated: 2016/02/01 12:57:19 by rortega          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: clean fclean re

FILES = init main copy_dir lst_files list_dir long_display long_display_suite list_all_sub_dir print_colors \
		handle_permissions handle_error params free_memory sort print_error count print_columns max_len lst_files_sort

SRC = $(addsuffix .c, $(FILES))
OBJS = $(addsuffix .o, $(FILES))
LIB = libft/libft.a libft/printf/libftprintf.a
NAME = ft_ls
CC = gcc
FLAGS = -Wall -Wextra -Werror -O3
INC = -I includes/

all: $(NAME)

$(NAME): $(SRC)
	@ echo "printf:  \033[32mcreating\033[0m  \033[36mlibrary\033[0m"
	@ make -C libft/printf/
	@ echo "libft:  \033[32mcreating\033[0m  \033[36mlibrary\033[0m"
	@ make -C libft/
	@ echo "ft_ls:  \033[32mcreating\033[0m  \033[33mbinary\033[0m"
	@ $(CC) $(FLAGS) $(INC) $(LIB) $(SRC) -o $(NAME)

relib:
	@ echo "printf: \033[31mremoving\033[0m  \033[36mlibrary\033[0m"
	@ echo "printf: \033[32mcreating\033[0m  \033[36mlibrary\033[0m"
	@ make re -C libft/printf/
	@ echo "libft: \033[31mremoving\033[0m  \033[36mlibrary\033[0m"
	@ echo "libft: \033[32mcreating\033[0m  \033[36mlibrary\033[0m"
	@ make re -C libft/
	@ echo "\033[33mdone.\033[0m"

clean:
	@ echo "libft: \033[31mremoving\033[0m  objects"
	@ make clean -C libft/
	@ echo "printf: \033[31mremoving\033[0m  objects"
	@ make clean -C libft/printf/
	@ echo "ft_ls: \033[31mremoving\033[0m  objects"
	@ rm -f $(OBJS)

fclean: clean
	@ echo "libft: \033[31mremoving\033[0m  \033[36mlibrary\033[0m"
	@ make fclean -C libft/
	@ echo "printf: \033[31mremoving\033[0m  \033[36mlibrary\033[0m"
	@ make fclean -C libft/printf/
	@ echo "ft_ls: \033[31mremoving\033[0m  \033[33mbinary\033[0m"
	@ rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
