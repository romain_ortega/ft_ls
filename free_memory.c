/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_helper.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 16:27:00 by rortega           #+#    #+#             */
/*   Updated: 2016/01/14 16:31:03 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"

void	free_path(t_data p, t_list *prev)
{
	if (p.d.path_set == false || p.d.nb_path == 0 || prev == NULL)
		return ;
	lst_del_one(&prev, ft_bzero);
}

void	free_error(t_error e, size_t index)
{
	if (e.error_set == false || e.nb_error == 0 || index >= e.nb_error)
		return ;
	if (e.error[index])
	{
		ft_strclr(e.error[index]);
		free(e.error[index]);
	}
	if (e.file[index])
	{
		ft_strclr(e.file[index]);
		free(e.file[index]);
	}
	if (e.display_top)
		free(e.display_top);
}

void	free_tab(void **file_infos, size_t size)
{
	size_t i;

	if (!file_infos || size == 0)
		return ;
	i = 0;
	while (i < size)
		free(file_infos[i++]);
	free(file_infos);
}

void	free_str_tab(char **tab, size_t size)
{
	size_t i;

	if (!tab || size == 0)
		return ;
	i = 0;
	while (i < size)
	{
		if (tab[i])
			ft_strdel(&tab[i]);
		i++;
	}
	free(tab);
}
