/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_dir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 16:40:06 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:16:08 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "ft_printf.h"
#include "libft.h"

static void		display(t_data *p, size_t i, size_t nb_columns)
{
	if ((p->d.dir_content->file_name[0] != '.' || p->o.f_a)
	|| (ft_strcmp(p->d.dir_content->file_name, ".") != 0
	&& ft_strcmp(p->d.dir_content->file_name, "..") != 0 && p->o.f_big_a))
	{
		print_colors(*p);
		ft_putstr(p->d.dir_content->file_name);
		reset_color();
		print_type_indicator(*p);
		if (((i + 1) % nb_columns) == 0)
			ft_putchar('\n');
		else
			print_space_0(*p);
	}
}

static void		display_dir_column(t_data *p)
{
	struct winsize 	w;
	size_t 			i 			= 0;
	size_t			nb_lines 	= 0;
	size_t			nb_columns 	= 0;
	size_t			max_len 	= (p->d.dir_content->error ? 0 : get_max_len(*p, 5));

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	if (max_len)
	{
		nb_columns = (w.ws_col - 5) / max_len;
		nb_lines = p->d.nb_dir_elem / nb_columns;
	}
	while (p->d.dir_content)
	{
		if (p->d.dir_content->error == NULL)
			display(p, i, nb_columns);
		else
			write(2, p->d.dir_content->error,
				ft_strlen(p->d.dir_content->error));
		p->d.dir_content = p->d.dir_content->next;
		i++;
	}
	if (p->d.nb_dir_elem > 2)
		ft_putchar('\n');
}

static void		display_dir(t_data p)
{
	while (p.d.dir_content)
	{
		if (p.d.dir_content->error == NULL)
		{
			if ((p.d.dir_content->file_name[0] != '.' || p.o.f_a)
			|| (ft_strcmp(p.d.dir_content->file_name, ".") != 0
			&& ft_strcmp(p.d.dir_content->file_name, "..") != 0 && p.o.f_big_a))
			{
				print_infos(p);
				print_colors(p);
				ft_putstr(p.d.dir_content->file_name);
				reset_color();
				print_type_indicator(p);
				if (p.d.dir_content->type == 'l' && p.o.f_l)
					print_link(p);
				ft_putchar('\n');
			}
		}
		else
			write(2, p.d.dir_content->error, ft_strlen(p.d.dir_content->error));
		p.d.dir_content = p.d.dir_content->next;
	}
}

void			display_dir_content(t_data p, char *path)
{
	static size_t 	v 			= 0;
	t_file_list		*begin_list = p.d.dir_content;

	if (p.d.dir_content == NULL || path == NULL)
		return ;
	begin_list = p.d.dir_content;
	v++;
	if (p.d.nb_path > 1 && (ft_strcmp(path, p.d.dir_content->file_name) != 0
	|| path[0] == '.'))
		(v <= 1 ? ft_printf("%s:\n", path) : ft_printf("\n\033[0m%s:\n", path));
	if (p.d.dir_content->error == NULL)
		print_total(p);
	if (p.d.nb_dir_elem >= 1 || (!p.e.error_set && p.d.dir_content->error))
	{
		if (p.o.f_1 || p.o.f_l || p.o.f_big_r)
			display_dir(p);
		else
			display_dir_column(&p);
	}
	else
	{
		write(2, p.d.dir_content->error, ft_strlen(p.d.dir_content->error));
		write(2, "\n", 1);
	}
	if (begin_list)
		lst_file_del(&begin_list);
}

void			list_dir(t_data p)
{
	size_t		i			= 0;
	size_t		total		= 0;
	size_t		n			= (p.d.path_set == true ? p.d.nb_path : 1);
	t_list 		*begin_list	= (p.d.path_set ? p.d.path : NULL);

	if (p.e.error_set)
		display_top_errors(p.e);
	total = p.d.nb_dir_elem;
	while (i < n)
	{
		copy_dir_content(&p, p.d.path->content);
		if (!p.o.f_f || p.d.nb_dir_elem < 2)
			sort_dir_content(&p);
		display_dir_content(p, p.d.path->content);
		get_max_len(p, 0);
		if (p.d.path->next)
			p.d.path = p.d.path->next;
		total += p.d.nb_dir_elem;
		i++;
	}
	if (total > 1)
		ft_printf("\nTotal: %zu files.\n", total);
	lst_del((begin_list == NULL ? &p.d.path : &begin_list), bzero);
}
