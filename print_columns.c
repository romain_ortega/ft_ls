/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_columns.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 08:05:18 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:26:44 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"
#include "ft_printf.h"

void				print_space_0(t_data p)
{
	size_t		len_cur 	= ft_strlen(p.d.dir_content->file_name);
	size_t		len_max 	= get_max_len(p, 5);
	int			nb_spaces 	= 1 + len_max - len_cur;
	size_t		i			= -1;
	char 		*s			= ft_strnew(nb_spaces + 1);

	while (nb_spaces--)
		s[++i] = ' ';
	if (p.o.f_p && !p.o.f_big_f && p.d.dir_content->type != 'd')
		ft_putchar(' ');
	else if (p.o.f_big_f && p.d.dir_content->type == '-')
		ft_putchar(' ');
	ft_putstr(s);
	ft_strdel(&s);
}

void				print_space_inode(t_data p)
{
	size_t		len_cur		= ft_intlen(p.d.dir_content->file_infos->st_ino);
	size_t	 	len_max		= get_max_len(p, 4);
	int			nb_spaces	= len_max - len_cur;
	size_t		i 			= 0;
	char 		*s			= ft_strnew(nb_spaces + 1);

	while (nb_spaces--)
	{
		s[i] = ' ';
		i++;
	}
	ft_putstr(s);
	ft_strdel(&s);
}

void				print_space_1(t_data p)
{
	size_t		len_cur		= ft_intlen(p.d.dir_content->file_infos->st_nlink);
	size_t		len_max		= get_max_len(p, 1);
	int			nb_spaces	= len_max - len_cur + 1;
	size_t		i			= 0;
	char 		*s			= ft_strnew(nb_spaces + 1);

	while (nb_spaces--)
	{
		s[i] = ' ';
		i++;
	}
	ft_putstr(s);
	ft_strdel(&s);
}

void				print_space_2(t_data p)
{
	size_t			len_cur		= 0;
	size_t			len_max		= get_max_len(p, 2);
	int				nb_spaces	= 0;
	struct passwd 	*pwuid		= NULL;
	size_t			i			= 0;
	char 			*s			= NULL;

	if (!(pwuid = getpwuid(p.d.dir_content->file_infos->st_uid)))
		len_cur = ft_intlen(p.d.dir_content->file_infos->st_uid);
	else
		len_cur = ft_strlen(pwuid->pw_name);
	nb_spaces = len_max - len_cur + 2;
	s = ft_strnew(nb_spaces + 1);
	while (nb_spaces--)
	{
		s[i] = ' ';
		i++;
	}
	ft_putstr(s);
	ft_strdel(&s);
}

void				print_space_3(t_data p)
{
	size_t			len_cur		= 0;
	size_t			len_max		= get_max_len(p, 3);
	int				nb_spaces	= 0;
	struct group 	*grgid		= NULL;
	size_t			i			= 0;
	char 			*s			= NULL;

	if (!(grgid = getgrgid(p.d.dir_content->file_infos->st_gid)))
		len_cur = ft_intlen(p.d.dir_content->file_infos->st_gid);
	else
		len_cur = ft_strlen(grgid->gr_name);
	len_cur += ft_intlen(p.d.dir_content->file_infos->st_size);
	nb_spaces = len_max - len_cur + 2;
	s = ft_strnew(nb_spaces + 1);
	while (nb_spaces--)
	{
		s[i] = ' ';
		i++;
	}
	ft_putstr(s);
	ft_strdel(&s);
}
