/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_display.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 13:58:44 by rortega           #+#    #+#             */
/*   Updated: 2016/01/22 13:59:14 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"
#include "ft_printf.h"

void	print_infos(t_data p)
{
	if (p.o.f_i)
	{
		print_space_inode(p);
		ft_printf("%llu ", p.d.dir_content->file_infos->st_ino);
	}
	if (p.o.f_l)
	{
		print_permissions(p);
		print_space_1(p);
		ft_printf("%hu ", p.d.dir_content->file_infos->st_nlink);
		if (p.o.f_o != false)
			print_owner(p);
		else
			print_space_2(p);
		if (p.o.f_g != false)
			print_group(p);
		else
			print_space_3(p);
		if (p.d.dir_content->type != 'b' && p.d.dir_content->type != 'c')
			ft_printf("%lld ", p.d.dir_content->file_infos->st_size);
		else
			print_minor_major(p);
		print_date(p);
	}
}

void	print_total(t_data p)
{
	int			total;
	size_t		i;
	t_file_list *lst;
	
	if (!p.o.f_l || p.d.nb_dir_elem <= 2)
		return ;
	lst = p.d.dir_content;
	total = 0;
	i = 0;
	while (i < p.d.nb_dir_elem)
	{
		if (lst->file_name[0] != '.' || p.o.f_a)
			total += lst->file_infos->st_blocks;
		if (lst->next)
			lst = lst->next;
		i++;
	}
	ft_printf("total %d     (%zu files)\n", total, p.d.nb_dir_elem - 2);
}

void	print_minor_major(t_data p)
{
	ft_printf("%d, %d ", major(p.d.dir_content->file_infos->st_rdev),
	minor(p.d.dir_content->file_infos->st_rdev));
}

void	print_date(t_data p)
{
	time_t	ltime;
	time_t	now;

	now = time(0);
	ltime = p.d.dir_content->file_infos->st_mtime;
	if (ltime > now || now - ltime > 15778463)
		ft_printf("%.6s %.5s ", ctime(&ltime) + 4, ctime(&ltime) + 19);
	else
		ft_printf("%.12s ", ctime(&ltime) + 4);
}

void	print_link(t_data p)
{
	char	linkname[1024];
	char	*tmp;
	bool	join;
	ssize_t	r;

	join = (ft_strcmp(p.d.path->content, "/link") == 0) ? (false) : (true);
	if (join)
		tmp = join_path(p.d.dir_content->file_name, p.d.path->content);
	else
		tmp = p.d.dir_content->file_name;
	r = readlink(tmp, linkname, sizeof(linkname) - 1);
	if (r == -1)
	{
		ft_putstr(" -> readlink() error");
		return ;
	}
	linkname[r] = '\0';
	ft_printf(" -> %s", linkname);
	if (join)
		ft_strdel(&tmp);
}
