/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_permissions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 13:36:23 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:08:52 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"

static bool			has_extended_attr(char *path)
{
	int			count;

	count = listxattr(path, NULL, 0, XATTR_NOFOLLOW);
	return ((count > 0) ? true : false);
}

static bool			has_acl(char *file)
{
	acl_t	acl;
	bool	is_acl;

	acl = acl_get_file(file, ACL_TYPE_EXTENDED);
	is_acl = ((acl) ? true : false);
	acl_free((void *)acl);
	return (is_acl);
}

static void			print_permissions_suite2(t_data *p, char *rights)
{
	char 			*tmp = NULL;

	tmp = join_path(p->d.dir_content->file_name, p->d.path->content);
	if (p->d.dir_content->file_infos->st_mode & S_IXOTH)
		rights[8] = 'x';
	else
		rights[8] = '-';
	if (p->d.dir_content->file_infos->st_mode & S_ISVTX)
	{
		if (p->d.dir_content->file_infos->st_mode & 0100)
			rights[8] = 't';
		else
			rights[8] = 'T';
	}
	if (has_extended_attr(tmp))
		rights[9] = '@';
	else if (has_acl(tmp))
		rights[9] = '+';
	else
		rights[9] = ' ';
	ft_putstr(rights);
	if (ft_strncmp(rights, "rwxrwxrwx", 9) == 0
	&& p->d.dir_content->type == 'd')
		p->d.dir_content->type = 'o';
	ft_strdel(&tmp);
	ft_strdel(&rights);
}

static void			print_permissions_suite(t_data p, char *rights)
{
	rights[3] = (p.d.dir_content->file_infos->st_mode & S_IRGRP) ? 'r' : '-';
	rights[4] = (p.d.dir_content->file_infos->st_mode & S_IWGRP) ? 'w' : '-';
	if (p.d.dir_content->file_infos->st_mode & S_IXGRP)
		rights[5] = 'x';
	else
		rights[5] = '-';
	if (p.d.dir_content->file_infos->st_mode & S_ISGID)
	{
		if (p.d.dir_content->file_infos->st_mode & 0010)
			rights[5] = 's';
		else
			rights[5] = 'S';
	}
	rights[6] = (p.d.dir_content->file_infos->st_mode & S_IROTH) ? 'r' : '-';
	rights[7] = (p.d.dir_content->file_infos->st_mode & S_IWOTH) ? 'w' : '-';
	print_permissions_suite2(&p, rights);
}

void				print_permissions(t_data p)
{
	char			*rights;

	rights = ft_strnew(11);
	rights[10] = '\0';
	ft_putchar(p.d.dir_content->type);
	rights[0] = (p.d.dir_content->file_infos->st_mode & S_IRUSR) ? 'r' : '-';
	rights[1] = (p.d.dir_content->file_infos->st_mode & S_IWUSR) ? 'w' : '-';
	if (p.d.dir_content->file_infos->st_mode & S_IXUSR)
		rights[2] = 'x';
	else
		rights[2] = '-';
	if (p.d.dir_content->file_infos->st_mode & S_ISUID)
	{
		if (p.d.dir_content->file_infos->st_mode & 0100)
			rights[2] = 's';
		else
			rights[2] = 'S';
	}
	print_permissions_suite(p, rights);
}
