/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 11:45:43 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 14:24:33 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	size_t			i;
	unsigned char	*mem;

	mem = NULL;
	i = 0;
	mem = (unsigned char*)malloc(size);
	if (!mem)
		return (NULL);
	ft_bzero(mem, size);
	return ((void *)mem);
}
