/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:04:34 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 17:16:15 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_countstr(char *s, char c)
{
	char	in_substr;
	size_t	nb_occ;

	nb_occ = 0;
	in_substr = 0;
	while (*s)
	{
		if (*s == c)
			in_substr = 0;
		else if (*s != c && !in_substr)
		{
			nb_occ++;
			in_substr = 1;
		}
		s++;
	}
	return (nb_occ);
}

static char		*ft_nextstr(char **tab, char c)
{
	char	*end;
	char	*result;
	size_t	len;

	while (**tab == c)
		(*tab)++;
	end = ft_strchr(*tab, c);
	if (NULL == end)
		len = ft_strlen(*tab);
	else
		len = end - *tab;
	result = ft_strsub(*tab, 0, len);
	*tab += len;
	return (result);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t	nb_str;
	size_t	i;
	char	**result;
	char	*p;

	p = (char*)s;
	nb_str = ft_countstr(p, c);
	result = (char**)malloc(sizeof(char*) * (nb_str + 1));
	if (!result)
		return (NULL);
	i = 0;
	while (i < nb_str)
	{
		result[i] = ft_nextstr(&p, c);
		i++;
	}
	return (result);
}

char			*ft_strunsplit(int ac, char **av)
{
	char	*str;

	INIT(int, i, 1);
	INIT(size_t, size, 0);
	while (i <= ac)
	{
		size += ft_strlen(av[i]);
		i++;
	}
	if (!(str = (char *)malloc(size + 1 + i)))
		return (NULL);
	bzero(str, size + 1 + i);
	i = 1;
	while (i <= ac)
	{
		ft_strcat(str, av[i]);
		ft_strcat(str, " ");
		i++;
	}
	return (str);
}
