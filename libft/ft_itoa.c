/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:03:43 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:50:26 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_intlen(long long n)
{
	int	len;

	if (n == 0)
		return (1);
	len = 0;
	while (n != 0)
	{
		len++;
		n = n / 10;
	}
	return (len);
}

char		*ft_itoa(int n)
{
	int		i;
	int		sign;
	char	*s;

	if (n == -2147483648)
		return ("-2147483648");
	sign = (n < 0) ? 1 : 0;
	i = ft_intlen(n);
	s = (char *)malloc(sizeof(char) * (i + sign + 1));
	if (s)
	{
		s = s + i + sign;
		*s = '\0';
		if (!n)
			*--s = '0';
		while (n != 0)
		{
			*--s = FT_ABS(n) % 10 + '0';
			n = n / 10;
		}
		if (sign == 1)
			*--s = '-';
	}
	return (s);
}
