/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 12:37:06 by rortega           #+#    #+#             */
/*   Updated: 2015/11/24 15:25:21 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *str)
{
	char	*s2;

	if (str)
	{
		s2 = ft_strnew(sizeof(char) * ft_strlen((char *)str) + 1);
		if (s2)
		{
			ft_strcpy(s2, str);
			return (s2);
		}
	}
	return (NULL);
}
