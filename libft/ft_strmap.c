/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 11:56:45 by rortega           #+#    #+#             */
/*   Updated: 2015/11/26 12:10:05 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *str, char (*f)(char))
{
	char	*s;
	size_t	i;

	i = 0;
	s = (char *)malloc(ft_strlen((char *)str + 1));
	if (!s)
		return (NULL);
	if (f)
	{
		while (str[i])
		{
			s[i] = f(str[i]);
			i++;
		}
	}
	s[i] = '\0';
	return (s);
}
