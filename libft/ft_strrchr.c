/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 16:53:21 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 15:01:28 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	char	*s;
	int		i;
	int		count;

	i = 0;
	count = 0;
	s = (char *)str;
	while (s[i])
	{
		if (s[i] == (char)c)
			count++;
		i++;
	}
	i = 0;
	while (*s && i < count)
	{
		if (*s == c)
			i++;
		s++;
	}
	if (i == count && count > 0)
		return (s - 1);
	if (!c)
		return ((char *)(str + ft_strlen((char *)str)));
	return (NULL);
}

char	*ft_strrcut(const char *str, int c)
{
	char	*s;

	if (!str)
		return (NULL);
	s = (char *)str;
	if (str[ft_strlen((char *)str) - 1] == c)
		s[ft_strlen((char *)str) - 1] = '\0';
	if (!(s = ft_strchr(ft_strrev(str), c)))
		return (NULL);
	return (ft_strrev((const char *)s));
}

char	*ft_strcut(const char *str, int c)
{
	char	*s;

	if (!(s = ft_strrchr(ft_strrev(str), c) + 1))
		return (NULL);
	return (ft_strrev((const char *)s));
}

int		ft_strichr(const char *str, int c)
{
	if (!str || !c)
		return ((int)ft_strlen((char *)str));
	return ((int)ft_strlen((char *)str) - (int)ft_strlen(ft_strchr(str, c)));
}
