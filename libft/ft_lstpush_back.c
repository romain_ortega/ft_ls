/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpush_back.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 12:39:45 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:38:21 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	lst_push_back(t_list *lst, t_list *elem)
{
	if (!lst || !elem)
		return ;
	while (lst->next != NULL)
		lst = lst->next;
	lst->next = elem;
}
