/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 16:37:42 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 15:00:55 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *str1, const char *str2, size_t l)
{
	char	*s1;
	char	*s2;
	size_t	i;

	i = ft_strlen((char *)str2);
	s1 = (char *)str1;
	s2 = (char *)str2;
	if (!l)
		return (NULL);
	if (!ft_strlen((char *)s1) && !i)
		return ("");
	if (!s2)
		return (s1);
	l = l - i;
	while (l-- && *s1 != '\0' && ft_strncmp(s1, s2, i) != 0)
		s1++;
	if (ft_strncmp(s1, s2, i) == 0)
		return (s1);
	return (NULL);
}
