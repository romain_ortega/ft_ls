/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:17 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 14:56:14 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *str, int c, size_t n)
{
	unsigned char	*ptr;
	unsigned char	d;

	ptr = (unsigned char *)str;
	d = (unsigned char)c;
	while (n--)
	{
		*ptr = d;
		ptr++;
	}
	return (str);
}
