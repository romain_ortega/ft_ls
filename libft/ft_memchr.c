/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:57:30 by rortega           #+#    #+#             */
/*   Updated: 2015/11/24 16:55:42 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *str, int c, size_t n)
{
	char			*ptr;
	unsigned char	d;

	d = (unsigned char)c;
	ptr = (char *)str;
	while (n--)
	{
		if (*ptr == (char)d)
			return (ptr--);
		ptr++;
	}
	return (NULL);
}
