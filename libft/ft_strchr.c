/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 16:53:36 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 14:58:00 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *str, int c)
{
	char *s;

	s = (char *)str;
	if (s)
	{
		while (*s && *s != (char)c)
			s++;
		if (*s == (char)c)
			return (s);
	}
	return (NULL);
}
