/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_void.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/11 17:25:16 by rortega           #+#    #+#             */
/*   Updated: 2015/12/11 17:25:19 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	ft_printvoid(void *data, t_flags p, size_t i)
{
	p.length = ft_baselen((long int)data, 16) + 2;
	i += (!p.f_minus ? v(p) : 0);
	i += write(1, "0x", 2);
	if (p.longueur == 0 && p.precision_set && (data == NULL || (int)data == 0))
		return (i);
	else if (p.precision_set)
		while (p.longueur > p.length - 2)
		{
			i += ft_printchar('0');
			p.longueur--;
		}
	else if (p.f_zero)
		while (p.longueur > p.length)
		{
			i += ft_printchar('0');
			p.longueur--;
		}
	i += ft_printbase((long int)data, 16, "0123456789abcdef");
	if (p.f_minus)
		while (p.longueur > p.length)
		{
			i += ft_printchar(' ');
			p.longueur--;
		}
	return (i);
}
