/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 17:04:09 by rortega           #+#    #+#             */
/*   Updated: 2016/01/10 17:04:20 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

long int		routes(t_flags p, unsigned char t, void *arg)
{
	if (t == 'c' && !p.m_l)
		return (c(&p, 0) + ft_printchar((char)arg) + c(&p, 1));
	if (t == 'C' || (t == 'c' && p.m_l))
		return (c(&p, 0) + ft_printwchar((int)arg) + c(&p, 1));
	if (p.precision_set && !p.precision && (intmax_t)arg == 0)
		return (p.f_space ? ft_putcharx(' ', p.longueur) : 0);
	if (t == 's' && !p.m_l)
		return (s(p, 0) + ft_printstr((char *)arg, p) + s(p, 1));
	if (t == 'S' || (t == 's' && p.m_l))
		return (s(p, 0) + ft_printwstr((wchar_t *)arg, p.longueur) + s(p, 1));
	if (t == 'p')
		return (ft_printvoid(arg, p, 0));
	return (c(&p, 0) + ft_printchar((unsigned char)t) + c(&p, 1));
}

long int		routes_m(t_flags p, unsigned char t, void *arg)
{
	if (p.precision_set && !p.precision && (long long)arg == 0)
		return (p.f_space ? ft_putcharx(' ', p.longueur) : 0);
	if (!p.m_set)
		return (ft_printnbr((int)arg, p));
	if (t == 'd' || t == 'i' || t == 'D' || t == 'I')
	{
		if ((p.m_l % 2) == 0 && !p.m_h)
			return (ft_printnbr((long long)arg, p));
		else if ((p.m_l % 2) != 0)
			return (ft_printnbr((long)arg, p));
		else if (p.m_j)
			return (ft_printnbr((intmax_t)arg, p));
		else if (p.m_z)
			return (ft_printnbr((ssize_t)arg, p));
		else if ((p.m_h % 2) != 0)
			return (ft_printnbr((short)arg, p));
		else if ((p.m_h % 2) == 0)
			return (ft_printnbr((signed char)arg, p));
	}
	return (0);
}

long int		routes_mu(t_flags p, unsigned char t, void *arg)
{
	if (p.precision_set && !p.precision && (unsigned long long)arg == 0)
		return (p.f_space ? ft_putcharx(' ', p.longueur) : 0);
	if (!p.m_set)
		return (u(p) + ft_printnbr_u((unsigned int)arg, p));
	if (t == 'u' || t == 'U')
	{
		if ((p.m_l % 2) == 0 && !p.m_h)
			return (u(p) + ft_printnbr_u((unsigned long long)arg, p));
		else if ((p.m_l % 2) != 0)
			return (u(p) + ft_printnbr_u((unsigned long)arg, p));
		else if (p.m_j)
			return (u(p) + ft_printnbr_u((uintmax_t)arg, p));
		else if (p.m_z)
			return (u(p) + ft_printnbr_u((size_t)arg, p));
		else if ((p.m_h % 2) != 0)
			return (u(p) + ft_printnbr_u((unsigned short)arg, p));
		else if ((p.m_h % 2) == 0)
			return (u(p) + ft_printnbr_u((unsigned char)arg, p));
		return (u(p) + ft_printnbr_u((unsigned int)arg, p));
	}
	return (0);
}

long int		routes_muo(t_flags p, unsigned char t, void *arg)
{
	if (!p.f_sharp && p.precision_set && !p.precision &&
		(unsigned long long)arg == 0)
		return (p.f_space ? ft_putcharx(' ', p.longueur) : 0);
	if (!p.m_set)
		return (b(p) + ft_printbase_u((unsigned int)arg, 8, "01234567"));
	if (t == 'o' || t == 'O')
	{
		if ((p.m_l % 2) == 0 && !p.m_h)
			return (b(p)
			+ ft_printbase_u((unsigned long long)arg, 8, "01234567"));
		else if ((p.m_l % 2) != 0)
			return (b(p) + ft_printbase_u((unsigned long)arg, 8, "01234567"));
		else if (p.m_j)
			return (b(p) + ft_printbase_u((uintmax_t)arg, 8, "01234567"));
		else if (p.m_z)
			return (b(p) + ft_printbase_u((size_t)arg, 8, "01234567"));
		else if ((p.m_h % 2) != 0)
			return (b(p) + ft_printbase_u((unsigned short)arg, 8, "01234567"));
		else if ((p.m_h % 2) == 0)
			return (b(p) + ft_printbase_u((unsigned char)arg, 8, "01234567"));
		return (b(p) + ft_printbase_u((unsigned int)arg, 8, "01234567"));
	}
	return (0);
}

long int		routes_mux(t_flags p, unsigned char t, void *arg)
{
	if (p.precision_set && !p.precision && (unsigned long long)arg == 0)
		return (p.f_space ? ft_putcharx(' ', p.longueur) : 0);
	if (!p.m_set)
		return (b(p) + ft_printbase_u((unsigned int)arg, 16, BASE(t)));
	if (t == 'x' || t == 'X')
	{
		if ((p.m_l % 2) == 0 && !p.m_h)
			return (b(p)
			+ ft_printbase_u((unsigned long long)arg, 16, BASE(t)));
		else if ((p.m_l % 2) != 0 && !p.m_h)
			return (b(p) + ft_printbase_u((unsigned long)arg, 16, BASE(t)));
		else if (p.m_j)
			return (b(p) + ft_printbase_u((uintmax_t)arg, 16, BASE(t)));
		else if (p.m_z)
			return (b(p) + ft_printbase_u((size_t)arg, 16, BASE(t)));
		else if ((p.m_h % 2) != 0)
			return (b(p) + ft_printbase_u((unsigned short)arg, 16, BASE(t)));
		else if ((p.m_h % 2) == 0)
			return (b(p) + ft_printbase_u((unsigned char)arg, 16, BASE(t)));
		return (b(p) + ft_printbase_u((unsigned int)arg, 16, BASE(t)));
	}
	return (0);
}
