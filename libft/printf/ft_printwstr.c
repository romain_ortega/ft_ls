/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printwstr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 17:03:58 by rortega           #+#    #+#             */
/*   Updated: 2016/01/10 17:04:00 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <string.h>

size_t			ft_wstrlen(wchar_t *s)
{
	size_t	i;
	size_t	n;

	i = 0;
	n = 0;
	if (!s)
		return (0);
	while (s[i])
	{
		n += ft_wcharlen(s[i]);
		i++;
	}
	return (n);
}

size_t			ft_wcharlen(unsigned int c)
{
	size_t	n;

	if (0x80 > c)
		return (1);
	n = 2;
	while (0x0000007ff < c)
	{
		n++;
		c >>= 5;
	}
	return (n);
}

static void		calculer_sequence(unsigned int c, unsigned char sequence[4])
{
	size_t	nb_octets;
	size_t	n;

	nb_octets = ft_wcharlen(c);
	if (c < 0x80)
	{
		sequence[0] = c;
		return ;
	}
	n = 0;
	if (nb_octets > 1)
		while (n < nb_octets)
		{
			*sequence >>= 1;
			*sequence |= 0x80;
			n++;
		}
	n = nb_octets;
	while (n--)
	{
		sequence[n] |= 0x80 | (c & 0x0000003f);
		c >>= 6;
	}
	sequence[0] |= c;
}

size_t			ft_printwchar(unsigned int c)
{
	size_t			i;
	int				n;

	FUCK_NORME;
	i = 0;
	calculer_sequence(c, sequence);
	n = 0;
	while (n < 4)
	{
		if (0 != sequence[n])
			i += write(1, &sequence[n], 1);
		n++;
	}
	return (i);
}

size_t			ft_printwstr(wchar_t *s, size_t len)
{
	size_t	i;
	size_t	n;

	i = 0;
	n = 0;
	if (s == NULL)
		return (write(1, "(null)", 6));
	while (s[n])
	{
		len--;
		i += ft_printwchar((unsigned int)s[n]);
		n++;
	}
	return (i);
}
