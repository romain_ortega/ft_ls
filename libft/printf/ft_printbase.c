/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printbase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/11 14:47:52 by rortega           #+#    #+#             */
/*   Updated: 2015/12/11 14:47:53 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t		ft_baselen(int64_t n, unsigned int base)
{
	size_t len;

	if (n == 0)
		return (1);
	len = 0;
	while (n != 0)
	{
		len++;
		n = n / base;
	}
	return (len);
}

size_t		ft_baselen_u(uintmax_t n, unsigned int base)
{
	size_t len;

	if (n == 0)
		return (1);
	len = 0;
	while (n != 0)
	{
		len++;
		n = n / base;
	}
	return (len);
}

size_t		ft_printbase_u(uintmax_t n, unsigned int base, char *s)
{
	size_t i;

	i = ft_baselen_u(n, base);
	if (n >= base)
	{
		ft_printbase(n / base, base, s);
		ft_printbase(n % base, base, s);
	}
	else
		ft_printchar(s[n]);
	return (i);
}

size_t		ft_printbase(int64_t n, unsigned int base, char *s)
{
	size_t i;

	if (n <= LONG_MIN || n <= (-9223372036854775807 - 1))
		return (ft_printf("%s", "9223372036854775808"));
	i = ft_baselen(n, base);
	if (n < 0)
		n = -n;
	if (n >= base)
	{
		ft_printbase(n / base, base, s);
		ft_printbase(n % base, base, s);
	}
	else
		ft_printchar(s[n]);
	return (i);
}
