/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   complete_suite.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 18:53:01 by rortega           #+#    #+#             */
/*   Updated: 2016/01/10 18:53:13 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	b_suite(t_flags *p, size_t *i)
{
	while (p->longueur > p->length)
	{
		*i += (p->f_zero && p->precision_set && !p->f_sharp
		? ft_printchar(' ') : 0);
		if (p->longueur > (p->precision + p->length))
			*i += ft_printchar(' ');
		p->longueur--;
	}
	while (p->precision > p->length)
	{
		*i += ft_printchar('0');
		p->precision--;
	}
}

size_t		b(t_flags p)
{
	size_t	i;

	i = 0;
	if (p.longueur > (p.length + p.precision))
	{
		if (p.f_sharp)
			p.longueur -= (p.c == 'x' || p.c == 'X' ? 2 : 1);
		while (p.longueur > p.length)
		{
			i += (p.f_zero ? ft_printchar('0') : ft_printchar(' '));
			p.longueur--;
		}
	}
	if (p.f_sharp)
	{
		p.precision += (p.precision_set && (p.c == 'x' || p.c == 'X') ? 1 : 0);
		if ((p.c == 'x' || p.c == 'X') && p.length > 0)
			i += ft_printf("0%c", p.c);
		else if ((p.c == 'o' || p.c == 'O') && p.length > 0)
			i += ft_printchar('0');
	}
	b_suite(&p, &i);
	return (i);
}
