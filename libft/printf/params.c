/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   params.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 17:01:50 by rortega           #+#    #+#             */
/*   Updated: 2016/01/10 17:01:52 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static size_t	set_value(const char *format, size_t *i)
{
	size_t size;

	size = 0;
	while (ft_isdigit(format[*i]))
	{
		if (size > 0 || format[*i] != '0')
		{
			size *= 10;
			size += (size_t)format[*i] - '0';
		}
		*i += 1;
	}
	return (size);
}

void			init_length(t_flags *p, void *arg)
{
	if (p->c == 'd' || p->c == 'D' || p->c == 'i')
		p->length = (p->c == 'D') ? (ft_intlen((long)arg))
		: (ft_intlen((int)arg));
	else if (p->c == 'u' || p->c == 'U')
		p->length = ft_intlen_u((unsigned long long)arg);
	else if (p->c == 'x' || p->c == 'X'
	|| p->c == 'o' || p->c == 'O')
	{
		if (p->f_sharp && (int)arg == 0)
			p->length = 0;
		else if (p->c == 'x' || p->c == 'X')
			p->length = ft_baselen((intmax_t)arg, 16);
		else
			p->length = ft_baselen((intmax_t)arg, 7);
	}
	else if (p->c == 'c' || p->c == 'C')
		p->length = (p->c == 'c' ? 1 : ft_wcharlen((wchar_t)arg));
	else if (p->c == 's' || p->c == 'S')
		p->length = (p->c == 's' ? ft_slen((char *)arg)
		: ft_wstrlen((wchar_t *)arg));
	else if (p->c == 'p')
		p->length = 15;
	else
		p->length = 1;
}

void			init_length_modifier(t_flags *p, char c)
{
	if (c == 'l')
	{
		p->m_set = true;
		p->m_l += 1;
	}
	else if (c == 'j')
	{
		p->m_set = true;
		p->m_j = true;
	}
	else if (c == 'z')
	{
		p->m_set = true;
		p->m_z = true;
	}
	else if (c == 'h')
	{
		p->m_set = true;
		p->m_h += 1;
	}
}

void			init_flag(t_flags *p, char c)
{
	if (c == '+')
	{
		p->f_plus = true;
		p->flag_set = true;
	}
	else if (c == '-')
	{
		p->f_minus = true;
		p->flag_set = true;
	}
	else if (c == ' ')
	{
		p->f_space = true;
		p->flag_set = true;
	}
	else if (c == '#')
	{
		p->f_sharp = true;
		p->flag_set = true;
	}
	else if (c == '0')
	{
		p->f_zero = true;
		p->flag_set = true;
	}
}

t_flags			init_format(const char *format, size_t i, size_t *n)
{
	t_flags p;

	init_param(&p);
	while (format[i] == '-' || format[i] == '+'
	|| format[i] == '0' || format[i] == '#' || format[i] == ' ')
	{
		init_flag(&p, format[i]);
		i++;
	}
	if (ft_isdigit(format[i]))
		p.longueur += set_value(format, &i);
	if (format[i] == '.')
	{
		i++;
		p.precision_set = true;
		p.precision += set_value(format, &i);
	}
	while (format[i] == 'l' || format[i] == 'h' || format[i] == 'j'
	|| format[i] == 'z')
	{
		init_length_modifier(&p, format[i]);
		i++;
	}
	*n += i;
	return (p);
}
