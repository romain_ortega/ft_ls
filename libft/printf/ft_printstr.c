/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/11 14:39:40 by rortega           #+#    #+#             */
/*   Updated: 2016/01/10 18:06:04 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	ft_slen(char *s)
{
	size_t i;

	i = 0;
	if (s)
	{
		while (s[i] != '\0')
			i++;
	}
	return (i);
}

int		ft_printchar(unsigned char c)
{
	return (write(1, &c, 1));
}

size_t	ft_putcharx(unsigned char c, size_t x)
{
	size_t i;

	i = 0;
	if (x)
		while (x--)
			i += ft_printchar(c);
	return (i);
}

size_t	ft_printstr(char *s, t_flags p)
{
	size_t	i;
	size_t	len;

	len = (p.length == 0 && !p.precision_set ? 6 : p.precision);
	i = 0;
	if (p.precision_set && !p.precision)
		return (0);
	if (!s)
		return (write(1, "(null)", len));
	while (s[i] && (i < len || len == 0))
	{
		ft_printchar(s[i]);
		i++;
	}
	return (i);
}
