/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/10 17:25:06 by rortega           #+#    #+#             */
/*   Updated: 2016/01/10 17:53:31 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

static long int	call_functions(unsigned char c, void *arg, t_flags p)
{
	if (c == '%')
		return (ft_printchar(c));
	p.c = c;
	init_length(&p, arg);
	if (p.precision_set && p.precision && p.f_sharp)
		p.precision--;
	if (p.precision && p.longueur > (p.precision - p.length))
		p.longueur -= p.precision - p.length;
	if ((c == 'd' || c == 'i') && !p.m_l)
		p.negative = ((int)arg < 0 ? (true) : (false));
	else if (((c == 'd' || c == 'i') && p.m_l) || c == 'D' || p.m_l > 1)
		p.negative = ((long long)arg < 0 ? (true) : (false));
	if (c == 'D' || c == 'I' || c == 'U' || c == 'O')
	{
		c += 32;
		p.m_set = true;
		p.m_l += 1;
	}
	if (c == 'd' || c == 'D' || c == 'i' || c == 'I')
		return (routes_m(p, p.c, arg));
	else if (p.c == 'x' || p.c == 'X')
		return (routes_mux(p, p.c, arg));
	else if (p.c == 'o' || p.c == 'O')
		return (routes_muo(p, p.c, arg));
	return (c == 'u' || c == 'U' ? routes_mu(p, c, arg) : routes(p, c, arg));
}

void			init_param(t_flags *p)
{
	p->c = 0;
	p->negative = 0;
	p->length = 0;
	p->precision_set = false;
	p->precision = 0;
	p->longueur_set = false;
	p->longueur = 0;
	p->flag_set = false;
	p->f_plus = false;
	p->f_minus = false;
	p->f_sharp = false;
	p->f_zero = false;
	p->f_space = false;
	p->m_set = false;
	p->m_h = 0;
	p->m_j = false;
	p->m_z = false;
	p->m_l = 0;
}

long int		ft_printf(const char *format, ...)
{
	va_list ap;
	size_t	i;
	size_t	len;
	t_flags	p;

	len = 0;
	i = 0;
	va_start(ap, format);
	while (format[i] != '\0')
	{
		if (format[i] == '%' && format[i + 1] != '\0')
		{
			i++;
			p = init_format(format + i, 0, &i);
			len += call_functions(format[i], va_arg(ap, void *), p);
		}
		else if (format[i] == '%')
			return (0);
		else
			len += ft_printchar(format[i]);
		i++;
	}
	va_end(ap);
	return (len);
}
