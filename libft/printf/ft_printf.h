/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/10 17:28:56 by rortega           #+#    #+#             */
/*   Updated: 2015/12/10 17:29:00 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# define TYPE_INT(c) (c == 'D' || c == 'd' || c == 'i' || c == 'I' ? 1 : 0)
# define TYPE_S(c) (c == 'c' || c == 'C' || c == 's' || c == 'S' ? 1 : 0)
# define TYPE_BASE(c) (c == 'x' || c == 'X' || c == 'o' || c == 'O' ? 1 : 0)
# define TYPE_UNSIGNED(c) (c == 'u' || c == 'U' ? 1 : 0)
# define BASE(c) (c == 'X' ? "0123456789ABCDEF" : "0123456789abcdef")
# define FUCK_NORME unsigned char	sequence[4] = {0};
# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>
# include <limits.h>
# include <stdbool.h>

typedef struct		s_flags
{
	char			c;
	bool			negative;
	size_t			length;
	bool			precision_set;
	size_t			precision;
	bool			longueur_set;
	size_t			longueur;
	bool			flag_set;
	bool			f_plus;
	bool			f_minus;
	bool			f_sharp;
	bool			f_zero;
	bool			f_space;
	bool			m_set;
	int				m_h;
	bool			m_j;
	bool			m_z;
	int				m_l;
}					t_flags;
long int			ft_printf(const char *format, ...);
long int			routes(t_flags p, unsigned char c, void *arg);
long int			routes_m(t_flags p, unsigned char c, void *arg);
long int			routes_mu(t_flags p, unsigned char c, void *arg);
long int			routes_muo(t_flags p, unsigned char c, void *arg);
long int			routes_mux(t_flags p, unsigned char c, void *arg);
void				init_length(t_flags *p, void *arg);
void				init_length_modifier(t_flags *p, char c);
void				init_flag(t_flags *p, char c);
void				init_param(t_flags *p);
t_flags				init_format(const char *format, size_t i, size_t *n);
size_t				ft_printnbr(int64_t n, t_flags p);
size_t				ft_printnbr_u(uintmax_t n, t_flags p);
int					ft_printfloat(double f, t_flags p);
size_t				ft_printbase(int64_t n, unsigned int base, char *s);
size_t				ft_printbase_u(uintmax_t n, unsigned int base, char *s);
size_t				ft_slen(char *s);
size_t				ft_printstr(char *s, t_flags p);
int					ft_printchar(unsigned char c);
size_t				ft_putcharx(unsigned char c, size_t x);
size_t				ft_wstrlen(wchar_t *s);
size_t				ft_wcharlen(unsigned int c);
size_t				ft_printwchar(unsigned int c);
size_t				ft_printwstr(wchar_t *s, size_t len);
size_t				ft_printvoid(void *data, t_flags p, size_t i);
size_t				ft_intlen(int64_t n);
size_t				ft_intlen_u(uintmax_t n);
size_t				ft_baselen(int64_t n, unsigned int base);
size_t				ft_baselen_u(uintmax_t n, unsigned int base);
size_t				b(t_flags p);
size_t				u(t_flags p);
size_t				s(t_flags p, int call);
size_t				d(t_flags p, size_t i);
size_t				v(t_flags p);
size_t				c(t_flags *p, size_t call);
int					ft_pow(int n, size_t p);
int					ft_isdigit(int c);
#endif
