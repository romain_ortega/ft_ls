/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printfloat.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/12 13:20:43 by rortega           #+#    #+#             */
/*   Updated: 2015/12/12 13:20:45 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_isdigit(int c)
{
	if (c >= 48 && c <= 57)
		return (c);
	return (0);
}

int	ft_pow(int n, size_t p)
{
	int	tmp;

	tmp = n;
	if (p == 0)
		return (1);
	while (p--)
		n *= tmp;
	return (n);
}

int	ft_printfloat(double f, t_flags p)
{
	long int len;

	p.precision = 5;
	len = 0;
	len += ft_printf("%d", (unsigned long long)f);
	f -= (int)f;
	len += ft_printf("%c", '.');
	f *= ft_pow(10, p.precision);
	len += ft_printf("%d\n", (int)f);
	return (len);
}
