/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_complete.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 17:02:28 by rortega           #+#    #+#             */
/*   Updated: 2016/01/10 17:02:30 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	v(t_flags p)
{
	size_t	i;

	i = 0;
	if (!p.precision_set && !p.f_zero)
		while (p.longueur > p.length)
		{
			i += ft_printchar(' ');
			p.longueur--;
		}
	return (i);
}

size_t	u(t_flags p)
{
	size_t i;

	i = 0;
	if (p.f_plus || p.f_minus)
		p.longueur = (p.longueur > p.length) ? (p.longueur - 1) : (p.longueur);
	while (p.longueur > p.length && !p.f_minus)
	{
		p.longueur--;
		if ((!p.flag_set || p.f_zero || p.f_plus || p.f_space || p.f_sharp)
		&& (!p.f_zero || p.precision_set))
			i += ft_printchar(' ');
		else
			i += ft_printchar('0');
	}
	while (p.precision > p.length)
	{
		p.precision -= 2;
		i += ft_printchar('0');
	}
	return (i);
}

size_t	d(t_flags p, size_t i)
{
	if ((p.f_plus && !p.negative) || p.f_minus)
		p.longueur = (p.longueur) ? (p.longueur - 1) : (p.longueur);
	if (p.negative && p.precision)
	{
		p.precision += 1;
		p.longueur -= (p.longueur ? 1 : 0);
	}
	while (p.longueur > p.length && !p.f_minus)
	{
		p.longueur--;
		if ((!p.flag_set || p.f_zero || p.f_plus || p.f_minus
		|| p.f_space || p.f_sharp) && (!p.f_zero || p.precision_set))
			i += ft_printchar(' ');
		else
			i += ft_printchar('0');
	}
	if ((((!p.f_zero || !p.flag_set) && !(p.f_zero && p.precision_set))
	&& p.negative) || (p.f_plus && !p.negative))
		i += (p.f_plus && !p.negative ? ft_printchar('+') : ft_printchar('-'));
	while (p.precision > p.length)
	{
		p.precision--;
		i += ft_printchar('0');
	}
	return (i);
}

size_t	s(t_flags p, int call)
{
	size_t i;

	i = 0;
	if (p.negative)
		p.longueur += 1;
	if (!p.length)
		p.length = ft_slen("(null)");
	if (p.precision_set && p.precision < p.longueur && p.longueur > p.length)
		p.longueur += (p.precision > p.length ? p.precision - p.length
			: p.length - p.precision);
	if (!p.f_minus && !call)
		while (p.longueur > p.length)
		{
			p.longueur--;
			i += (p.f_zero ? ft_printchar('0') : ft_printchar(' '));
		}
	if (p.f_minus && call)
		while (p.longueur > p.length)
		{
			p.longueur--;
			i += ft_printchar(' ');
		}
	return (i);
}

size_t	c(t_flags *p, size_t call)
{
	size_t i;

	i = 0;
	if (!p->f_minus && call == 0)
	{
		while (p->longueur > p->length)
		{
			p->longueur--;
			i += (p->f_zero ? ft_printchar('0') : ft_printchar(' '));
		}
	}
	if (p->f_minus && call == 1)
	{
		while (p->longueur > p->length)
		{
			i += (p->f_zero ? ft_printchar('0') : ft_printchar(' '));
			p->longueur--;
		}
	}
	return (i);
}
