/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printnbr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:18:46 by rortega           #+#    #+#             */
/*   Updated: 2015/11/26 10:19:20 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "stdio.h"

size_t			ft_intlen(int64_t n)
{
	size_t	sign;

	sign = (n < 0) ? (1) : (0);
	return (ft_baselen(n, 10) + sign);
}

size_t			ft_intlen_u(uintmax_t n)
{
	return (ft_baselen_u(n, 10));
}

size_t			ft_printnbr_u(uintmax_t n, t_flags p)
{
	size_t			i;
	unsigned int	tmp;

	i = 0;
	if (!p.f_minus)
		u(p);
	tmp = p.precision;
	p.precision = 0;
	p.longueur = 0;
	if (n >= 10)
	{
		ft_printnbr_u(n / 10, p);
		ft_printnbr_u(n % 10, p);
	}
	else
	{
		ft_printchar(n + '0');
		if (p.f_minus)
		{
			p.precision = tmp;
			u(p);
			p.precision = 0;
		}
	}
	return (p.length + i);
}

static size_t	handle_flag(int64_t *n, t_flags *p, size_t i)
{
	if ((*n < 0 && (p->f_zero && !p->precision_set))
	|| (!p->negative && *n < 0))
		i += ft_printchar('-');
	if (!p->f_minus || (p->f_minus && p->precision > p->longueur))
		i += d(*p, 0);
	else if (p->f_plus && !p->f_zero)
		i += ft_printchar('+');
	else if (p->negative)
		i += ft_printchar('-');
	if (*n < 0 && (p->f_zero && p->precision_set))
		i += ft_printchar('-');
	if (*n < 0)
		*n = -*n;
	return (i);
}

size_t			ft_printnbr(int64_t n, t_flags p)
{
	size_t			i;

	i = 0;
	if (n == LLONG_MAX && (p.m_z || p.m_j))
		return (ft_printf("%lld", n));
	if ((p.f_plus && n < 0 && p.longueur && p.longueur < p.length)
	|| (n > 0 && p.f_space && !p.longueur && !p.precision))
		return (ft_printf(" %d", n));
	if (p.f_space && p.longueur < p.precision)
		i += (p.precision > p.length && p.longueur > 0 ? 0 : ft_printchar(' '));
	else if (p.f_space && n <= 0)
		return (ft_printf("%d", n));
	i += handle_flag(&n, &p, 0) + ft_printbase(n, 10, "0123456789");
	if (p.f_plus)
		p.longueur--;
	if (p.f_minus && p.longueur > p.length)
	{
		while (p.longueur > p.length)
		{
			i += ft_printchar(' ');
			p.longueur--;
		}
	}
	return (i);
}
