/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 17:25:16 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 17:38:25 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char	*ft_strjoin(char const *str1, char const *str2)
{
	char	*s;
	size_t	i;

	i = 0;
	s = ft_strnew(ft_strlen((char *)str1) + ft_strlen((char *)str1) + 1);
	if (!s)
		return (NULL);
	if (!*str1 && !*str2)
		return ("\0");
	ft_strcpy(s, str1);
	ft_strcat(s, str2);
	return (s);
}
