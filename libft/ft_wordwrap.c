/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wordwrap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 18:15:47 by rortega           #+#    #+#             */
/*   Updated: 2015/11/27 18:16:17 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_nobr(char *dest, char const *src, size_t n, size_t nb)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	while (src[i])
	{
		if (src[i] != '\n')
		{
			if (nb == n)
			{
				dest[j] = '\n';
				nb = 0;
			}
			else
			{
				dest[j] = src[i];
				nb++;
			}
			j++;
		}
		i++;
	}
	dest[j] = '\0';
	return (dest);
}

char		*ft_wordwrap(const char *str, size_t n)
{
	char	*s;
	size_t	i;

	if (!str || !n)
		return (NULL);
	i = 0;
	s = (char *)malloc(ft_strlen((char *)str) + 1 + ft_strlen((char *)str) / n);
	if (!s)
		return (NULL);
	return (ft_nobr(s, str, n, 0));
}
