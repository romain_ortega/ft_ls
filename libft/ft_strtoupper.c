/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strupcase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:59:15 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:09:58 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtoupper(const char *str)
{
	char	*s;
	size_t	i;

	if (!str)
		return (NULL);
	i = 0;
	s = (char *)malloc(ft_strlen((char *)str) + 1);
	*s = (char)ft_toupper(str[i]);
	s++;
	i++;
	while (str[i])
	{
		if (str[i - 1] != '.' || str[i - 2] != '.')
			*s = (char)ft_tolower(str[i]);
		else
			*s = (char)ft_toupper(str[i]);
		s++;
		i++;
	}
	*s = '\0';
	s -= ft_strlen((char *)str);
	return (s);
}
