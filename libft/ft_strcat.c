/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 13:57:59 by rortega           #+#    #+#             */
/*   Updated: 2015/11/26 10:29:00 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *dest, const char *src)
{
	char *d;
	char *s;

	d = (char *)dest;
	s = (char *)src;
	ft_memcpy(d + ft_strlen(d), s, sizeof(char) * (ft_strlen(s) + 1));
	return (d);
}
