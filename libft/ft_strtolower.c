/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtolower.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:45:07 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:10:28 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtolower(const char *str)
{
	char	*s;
	size_t	i;

	if (!str)
		return (NULL);
	i = 0;
	s = (char *)malloc(ft_strlen((char *)str) + 1);
	while (str[i])
	{
		*s = (char)ft_tolower(str[i]);
		s++;
		i++;
	}
	*s = '\0';
	s -= ft_strlen((char *)str);
	return (s);
}
