/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 18:01:32 by rortega           #+#    #+#             */
/*   Updated: 2015/11/27 15:25:08 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t l)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	while (i < l && dst[i])
		i++;
	if (i == l)
		return (ft_strlen((char *)src) + l);
	while (l > (i + j + 1) && src[j])
	{
		dst[i + j] = src[j];
		j++;
	}
	dst[i + j] = '\0';
	if (i + j + 1 == l)
		j = ft_strlen((char *)src);
	return (i + j);
}
