/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 14:58:40 by rortega           #+#    #+#             */
/*   Updated: 2015/11/27 14:59:10 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	swap_elem(t_list *elem1, t_list *elem2)
{
	void	*tmp;

	tmp = elem1->content;
	elem1->content = elem2->content;
	elem2->content = tmp;
}

t_list	*lst_map(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*first;
	t_list	**list;
	size_t	i;

	i = 0;
	list = NULL;
	if (lst && f)
		while (lst->next)
		{
			lst = f(lst);
			if (i == 0)
			{
				i = 1;
				first = lst_new(lst->content, lst->content_size);
			}
			else
				(*list)->next = lst_new(lst->content, lst->content_size);
			if ((*list)->next && first)
				*list = (*list)->next;
			else
				return (NULL);
			lst = lst->next;
		}
	return ((!lst || !f) ? NULL : first);
}
