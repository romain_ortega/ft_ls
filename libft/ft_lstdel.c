/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 14:36:47 by rortega           #+#    #+#             */
/*   Updated: 2015/11/27 14:37:20 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	lst_del(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*next;

	if (!alst)
		return ;
	while (*alst)
	{
		next = (*alst)->next;
		if ((*alst)->content)
			lst_del_one(alst, del);
		*alst = next;
	}
}
