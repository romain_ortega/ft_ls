/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 11:58:30 by rortega           #+#    #+#             */
/*   Updated: 2015/11/26 12:12:14 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *str, char (*f)(unsigned int, char))
{
	char	*s;
	size_t	i;

	i = 0;
	s = (char *)malloc(ft_strlen((char *)str + 1));
	if (!s)
		return (NULL);
	if (f)
	{
		while (str[i])
		{
			s[i] = f(i, str[i]);
			i++;
		}
	}
	s[i] = '\0';
	return (s);
}
