/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 14:44:00 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:14:10 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_ms		init(void)
{
	t_ms p;

	p.size = 1;
	p.num_merges = 0;
	p.left_size = 0;
	p.right_size = 0;
	p.tail = NULL;
	p.left = NULL;
	p.right = NULL;
	p.next = NULL;
	return (p);
}

static void		split_2(t_ms *p, int (*cmp)(t_list *, t_list *))
{
	if (!p->left_size)
	{
		p->next = p->right;
		p->right = p->right->next;
		p->right_size -= 1;
	}
	else if (!p->right_size || !p->right)
	{
		p->next = p->left;
		p->left = p->left->next;
		p->left_size -= 1;
	}
	else if (cmp(p->left, p->right) < 0)
	{
		p->next = p->left;
		p->left = p->left->next;
		p->left_size -= 1;
	}
	else
	{
		p->next = p->right;
		p->right = p->right->next;
		p->right_size -= 1;
	}
}

static void		split_1(t_ms *p)
{
	p->num_merges++;
	p->right = p->left;
	p->left_size = 0;
	p->right_size = p->size;
	while (p->right && p->left_size < p->size)
	{
		p->left_size++;
		p->right = p->right->next;
	}
}

static void		split_0(t_ms *p, t_list **list, int (*cmp)(t_list *, t_list *))
{
	split_1(p);
	while (p->left_size > 0 || (p->right_size > 0 && p->right))
	{
		split_2(p, cmp);
		if (p->tail)
			p->tail->next = p->next;
		else
			*list = p->next;
		p->tail = p->next;
	}
	p->left = p->right;
}

t_list			*lst_sort(t_list *list, int (*cmp)(t_list *, t_list *))
{
	INIT(t_ms, p, init());
	INIT(int, call, 0);
	if (list == NULL || list->next == NULL)
		return (list);
	while (p.num_merges > 1 || (call++) == 0)
	{
		p.num_merges = 0;
		p.left = list;
		list = NULL;
		p.tail = list;
		while (p.left)
			split_0(&p, &list, cmp);
		p.tail->next = 0;
		p.size <<= 1;
	}
	return (list);
}
