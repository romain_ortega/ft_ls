/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpush_data.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 12:39:45 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:38:21 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	lst_push_data(t_list **alst, void *data, size_t len)
{
	t_list *elem;

	if (!data || !len)
		return ;
	if (!alst)
		*alst = lst_new(data, len);
	else
	{
		elem = *alst;
		while (elem->next != NULL)
			elem = elem->next;
		elem->next = lst_new(data, len);
	}
}
