/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 17:45:51 by rortega           #+#    #+#             */
/*   Updated: 2015/11/25 17:47:25 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strnequ(char const *str1, char const *str2, size_t n)
{
	if (!n)
		return (1);
	n--;
	while (*str1 == *str2 && n--)
	{
		str1++;
		str2++;
		if (!*str1 && !*str2)
			return (1);
	}
	if (*str1 == *str2)
		return (1);
	return (0);
}
