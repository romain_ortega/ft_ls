/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quicksort_str.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 18:33:13 by rortega           #+#    #+#             */
/*   Updated: 2016/01/21 18:40:03 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	swap_str_ptr(char const **arg1, char const **arg2)
{
	const char *tmp;

	tmp = *arg1;
	*arg1 = *arg2;
	*arg2 = tmp;
}

void	quicksort_str(char const *args[], unsigned int len,
		int (*cmp)(char const *, char const *))
{
	unsigned int i;
	unsigned int pvt;

	pvt = 0;
	i = 0;
	if (len <= 1)
		return ;
	swap_str_ptr(args + ((unsigned int)rand() % len), args + len - 1);
	while (i < len - 1)
	{
		if (cmp(args[i], args[len - 1]) < 0)
			swap_str_ptr(args + i, args + pvt++);
		i++;
	}
	swap_str_ptr(args + pvt, args + len - 1);
	quicksort_str(args, pvt++, cmp);
	quicksort_str(args + pvt, len - pvt, cmp);
}
