/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstprev.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 14:44:00 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:14:10 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*lst_prev(t_list *lst, t_list *elem)
{
	while (lst)
	{
		if (elem == lst->next)
			return (lst);
		lst = lst->next;
	}
	return (NULL);
}
