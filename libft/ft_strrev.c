/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 17:16:13 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:11:04 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(const char *str)
{
	char	*s;
	char	*tmp;
	size_t	n;
	size_t	i;

	if (!str)
		return (NULL);
	i = ft_strlen((char *)str) + 1;
	n = 0;
	tmp = (char *)str;
	s = ft_strnew(i + 1);
	if (!s)
		return (NULL);
	while (i--)
	{
		s[n] = (char)tmp[i];
		n++;
	}
	s[n] = '\0';
	s -= i;
	return (s);
}
