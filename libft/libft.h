/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 17:16:53 by rortega           #+#    #+#             */
/*   Updated: 2016/01/21 18:41:50 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>
# include <string.h>
# include <unistd.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_ms
{
	int				size;
	int				num_merges;
	int				left_size;
	int				right_size;
	t_list			*tail;
	t_list			*left;
	t_list			*right;
	t_list			*next;
}					t_ms;

void				ft_bzero(void *str, size_t n);
void				*ft_memset(void *str, int c, size_t n);
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memchr(const void *str, int c, size_t n);
int					ft_memcmp(const void *str1, const void *str2, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t n);
char				*ft_strstr(const char *str, const char *rp);
char				*ft_strnstr(const char *str, const char *rp, size_t n);
char				*ft_strchr(const char *str, int c);
char				*ft_strrchr(const char *str, int c);
char				*ft_strrcut(const char *str, int c);
char				*ft_strcut(const char *str, int c);
int					ft_strichr(const char *str, int c);
int					ft_strcmp(const char *str1, const char *str2);
int					ft_strncmp(const char *str1, const char *str2, size_t n);
int					ft_atoi(const char *str);
size_t				ft_strlen(char *s);
char				*ft_strdup (const char *str);
char				*ft_strcpy(char *dest, const char *src);
char				*ft_strncpy(char *dest, const char *src, size_t n);
char				*ft_strcat(char *dest, const char *src);
size_t				ft_strlcat(char *dest, const char *src, size_t n);
char				*ft_strncat(char *dest, const char *src, size_t l);
int					ft_tolower(int c);
int					ft_toupper(int c);
int					ft_isalnum(int c);
int					ft_isalpha(int c);
int					ft_isascii (int c);
int					ft_isdigit(int c);
int					ft_isprint(int c);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striteri(char *str, void (*f)(unsigned int, char *));
void				ft_striter(char *str, void (*f)(char *));
char				*ft_strmap(char const *str, char (*f)(char));
char				*ft_strmapi(char const *s, char(*f)(unsigned int, char));
char				*ft_strtrim(char const *str);
char				**ft_strsplit(char const *s, char c);
char				*ft_strunsplit(int ac, char **av);
char				*ft_strsub(char const *str, unsigned int start, size_t len);
char				*ft_strjoin(char const *str1, char const *str2);
int					ft_strequ(char const *str1, char const *str2);
int					ft_strnequ(char const *str1, char const *str2, size_t n);
char				*ft_itoa(int n);
void				ft_putchar(char c);
void				ft_putstr(char const *str);
void				ft_putnbr(int n);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putendl(char const *s);
t_list				*lst_new(void const *content, size_t content_size);
void				lst_del_one(t_list **alst, void (*del)(void *, size_t));
void				lst_del(t_list **alst, void (*del)(void *, size_t));
void				lst_add(t_list **alst, t_list *new);
void				lst_push_back(t_list *lst, t_list *elem);
void				lst_push_data(t_list **alst, void *data, size_t len);
t_list				*lst_sort(t_list *lst, int (*cmp)(t_list *, t_list *));
void				lst_iter(t_list *lst, void (*f)(t_list *elem));
t_list				*lst_map(t_list *lst, t_list *(*f)(t_list *elem));
size_t				lst_size(t_list *lst);
t_list				*lst_prev(t_list *lst, t_list *elem);
char				*ft_strrev(const char *str);
int					ft_strncasecmp(const char *str1, const char *str2);
char				*ft_strtolower(const char *str);
char				*ft_strtoupper(const char *str);
char				*ft_wordwrap(const char *str, size_t n);
void				swap_str_ptr(char const **arg1, char const **arg2);
void				quicksort_str(char const *args[], unsigned int len,
					int (*cmp)(char const*, char const*));
int					invalid_input(int code, char *msg);
#endif
