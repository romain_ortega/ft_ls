/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:26:34 by rortega           #+#    #+#             */
/*   Updated: 2015/11/28 14:08:48 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(char const *str)
{
	char	*s;
	size_t	i;
	size_t	j;

	if (!str)
		return (NULL);
	i = 0;
	j = ft_strlen((char *)str);
	s = (char *)str;
	while ((' ' == s[i] || '\n' == s[i] || '\t' == s[i])
			&& s[i])
		i++;
	while ((' ' == s[j - 1] || '\n' == s[j - 1] || '\t' == s[j - 1])
			&& s[j - 1])
		j--;
	if (!j)
		return ("");
	return (ft_strsub(str, i, j - i));
}
