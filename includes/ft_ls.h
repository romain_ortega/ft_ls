/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 19:31:24 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:06:16 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H
# include <sys/xattr.h>
# include <grp.h>
# include <pwd.h>
# include <time.h>
# include <stdbool.h>
# include <sys/acl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <dirent.h>
# include <pwd.h>
# include <time.h>
# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <errno.h>
# include <sys/ioctl.h>
# include <limits.h>
# include "libft.h"
# ifndef WIN32
#  include <sys/types.h>
# endif

typedef struct			s_max_len
{
	size_t				len_max_1;
	size_t				len_max_2;
	size_t				len_max_3;
	size_t				size_max;
	size_t				len_max_4;
	size_t				len_max_5;
}						t_max_len;

typedef struct			s_file_list
{
	char				type;
	char				*file_name;
	char				*error;
	struct stat			*file_infos;
	struct s_file_list	*next;
}						t_file_list;

typedef struct			s_file_ms
{
	int					size;
	int					num_merges;
	int					left_size;
	int					right_size;
	t_file_list			*tail;
	t_file_list			*left;
	t_file_list			*right;
	t_file_list			*next;
}						t_file_ms;

typedef struct			s_dir
{
	bool				path_set;
	t_list				*path;
	size_t				nb_path;
	t_file_list			*dir_content;
	size_t				nb_dir_elem;
}						t_dir;

typedef struct			s_error
{
	bool				error_set;
	size_t				nb_error;
	bool				*display_top;
	char				*error[256];
	char				*file[256];
}						t_error;

typedef struct			s_opt
{
	bool				flag_set;
	bool				f_1;
	bool				f_l;
	bool				f_big_r;
	bool				f_a;
	bool				f_big_a;
	bool				f_t;
	bool				f_u;
	bool				f_c;
	bool				f_big_s;
	bool				f_i;
	bool				f_o;
	bool				f_f;
	bool				f_big_f;
	bool				f_g;
	bool				f_p;
}						t_opt;

typedef struct			s_data
{
	t_dir				d;
	t_opt				o;
	t_error				e;
}						t_data;

void					list_dir(t_data p);
void					list_all_sub_dir(t_data *p);
char					**list_dir_path(t_data *p, char *path, size_t *nb_dir);

t_file_list				*lst_new_file(struct dirent *file,
						struct stat *file_infos);
void					lst_push_new_file(t_file_list **alst, struct dirent
						*file, struct stat *file_infos);
void					lst_file_del(t_file_list **alst);

void					init_data(t_data *p);
t_data					parse_args(int ac, char **av);
void					parse_flag(char *f, t_data *p);

size_t					get_max_len(t_data p, int column);
size_t					count_dir_path(t_data *p, char *path);
void					copy_dir_content(t_data *p, char *path);
char					*join_path(char *file, char *path);
void					display_dir_content(t_data p, char *path);

void					print_infos(t_data p);
void					print_total(t_data p);
void					print_minor_major(t_data p);
void					print_owner(t_data p);
void					print_group(t_data p);
void					print_date(t_data p);
void					print_permissions(t_data p);
void					print_link(t_data p);
void					print_colors(t_data p);
void					print_space_0(t_data p);
void					print_space_1(t_data p);
void					print_space_2(t_data p);
void					print_space_3(t_data p);
void					print_space_inode(t_data p);
void					print_type_indicator(t_data p);
void					reset_color(void);

void					exit_usage(char opt);
void					exit_perror(void);
void					exit_perror_msg(char *msg);
void					exit_error(char *s);

void					display_top_errors(t_error e);
void					dup_error(t_data *p, char *path);
void					save_error(t_error *e, char *file);
void					sort_error(t_error *e, t_opt o);

void					free_path(t_data p, t_list *prev);
void					free_error(t_error e, size_t index);
void					free_tab(void **file_infos, size_t size);
void					free_str_tab(char **tab, size_t index);

t_list					*sort_path(t_data *p);
void					sort_dir_content(t_data *p);
void					lst_file_sort(t_file_list **list, char s,
						int (*cmp)(t_file_list *, t_file_list *, char s));
int						lst_cmp(t_list *elem1, t_list *elem2);
int						lst_file_cmp(t_file_list *e1, t_file_list *e2, char s);
#endif
