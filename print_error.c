/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:55:40 by rortega           #+#    #+#             */
/*   Updated: 2016/02/01 15:55:41 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "ft_printf.h"

void	exit_perror(void)
{
	perror("ft_ls");
	exit(-1);
}

void	exit_perror_msg(char *msg)
{
	ft_printf("ft_ls: %s: ", msg);
	perror("");
	exit(-1);
}

void	exit_usage(char opt)
{
	ft_printf("ft_ls: illegal option -- %c\nusage: ls ", opt);
	ft_printf("[-AFRSacfgiloptu1] [file ...]\n");
	exit(-1);
}

void	exit_error(char *s)
{
	write(2, s, ft_strlen(s));
	exit(-1);
}
