/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_all_sub_dir.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 18:15:50 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:12:35 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "ft_printf.h"
#include "libft.h"

static int	copy_sub_dir(t_data *p, char **path, size_t nb_path)
{
	size_t 	nb_dir 	= 0;
	size_t 	index 	= 0;
	size_t	n		= p->d.nb_path;

	while (index < nb_path)
	{
		if (ft_strcmp(ft_strchr(path[index], '/'), ".") != 0
		&& ft_strcmp(ft_strchr(path[index], '/'), "..") != 0)
		{
			copy_dir_content(p, path[index]);
			nb_dir++;
		}
		index++;
	}
	index = 0;
	while (index < nb_dir)
	{
		if (path[index])
		{
			lst_push_data(&p->d.path, path[index], ft_strlen(path[index]));
			n++;
		}
		index++;
	}
	free_str_tab(path, index);
	return (nb_dir);
}

char		**list_dir_path(t_data *p, char *path, size_t *nb_dir)
{
	char 			**dir 		= NULL;
	size_t 			n 			= 0;
	DIR 			*ret_dir	= NULL;
	struct dirent 	*file 		= NULL;

	if ((*nb_dir = count_dir_path(p, path)) == 0)
		return (NULL);
	if (!(dir = (char **)malloc(*nb_dir * sizeof(char *))))
		exit_error("error: malloc fail");
	if (!(ret_dir = opendir(path)))
		exit_perror();
	while ((file = readdir(ret_dir)) != NULL)
	{
		if (file->d_type == DT_DIR && (*file->d_name != '.' || p->o.f_a) &&
		ft_strcmp(file->d_name, ".") != 0 && ft_strcmp(file->d_name, "..") != 0)
		{
			dir[n] = ft_strnew(ft_strlen(file->d_name) + ft_strlen(path) + 2);
			ft_strcat(dir[n], path);
			ft_strcat(dir[n], "/");
			ft_strcat(dir[n], file->d_name);
			n++;
		}
	}
	if (closedir(ret_dir) == -1)
		exit_perror();
	return (dir);
}

void		list_all_sub_dir(t_data *p)
{
	size_t	nb_dir 		= 0;
	size_t	n			= 0;
	t_list 	*begin_list = p->d.path;

	while (p->d.path && (n = 0) == 0)
	{
		if (p->d.path->content)
		{
			nb_dir += copy_sub_dir(p,
						list_dir_path(p, p->d.path->content, &n), n);
			p->d.nb_path += n;
		}
		if (p->d.path->next)
			p->d.path = p->d.path->next;
		else
		{
			p->d.path = begin_list;
			break ;
		}
	}
	if (!p->d.path_set)
	{
		p->d.nb_path += 1;
		p->d.path_set = true;
	}
}
