/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:17:44 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:09:21 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		init_data(t_data *p)
{
	p->d.dir_content = NULL;
	p->d.path = NULL;
	p->d.path_set = false;
	p->d.nb_path = 0;
	p->d.nb_dir_elem = 0;
	p->e.error_set = false;
	p->e.nb_error = 0;
	p->o.flag_set = false;
	p->o.f_1 = false;
	p->o.f_l = false;
	p->o.f_big_r = false;
	p->o.f_big_s = false;
	p->o.f_a = false;
	p->o.f_big_a = false;
	p->o.f_t = false;
	p->o.f_u = false;
	p->o.f_c = false;
	p->o.f_i = false;
	p->o.f_o = false;
	p->o.f_f = false;
	p->o.f_big_f = false;
	p->o.f_g = false;
	p->o.f_p = false;
}

static void	parse_flag_suite(char *f, size_t i, t_data *p)
{
	if (f[i] == 'R')
		p->o.f_big_r = true;
	if (f[i] == 'a')
		p->o.f_a = true;
	if (f[i] == 'A')
		p->o.f_big_a = true;
	if (f[i] == 't')
		p->o.f_t = true;
	if (f[i] == 'u')
		p->o.f_u = true;
	if (f[i] == 'c')
		p->o.f_c = true;
	if (f[i] == 'S')
		p->o.f_big_s = true;
	if (f[i] == 'i')
		p->o.f_i = true;
	if (f[i] == 'p')
		p->o.f_p = true;
	if (f[i] == 'g')
	{
		p->o.f_g = true;
		p->o.f_l = true;
	}
	if (f[i] == '1')
		p->o.f_1 = true;
	if (f[i] == 'l')
	{
		p->o.f_l = true;
		p->o.f_o = true;
		p->o.f_g = true;
	}
	if (f[i] == 'o')
	{
		p->o.f_o = true;
		p->o.f_l = true;
	}
	if (f[i] == 'f')
	{
		p->o.f_f = true;
		p->o.f_a = true;
	}
	if (f[i] == 'F')
	{
		p->o.f_big_f = true;
		p->o.f_p = true;
	}
}

void		parse_flag(char *f, t_data *p)
{
	size_t i;

	i = 0;
	if (!f)
		return ;
	p->o.flag_set = true;
	while (f[i] != '\0')
	{
		parse_flag_suite(f, i, p);
		if (f[i] != 'l' && f[i] != 'R' && f[i] != 'a' && f[i] != 'c'
		&& f[i] != 'A' && f[i] != 't' && f[i] != 'i' && f[i] != 'o'
		&& f[i] != 'F' && f[i] != 'u' && f[i] != 'S' && f[i] != 'f'
		&& f[i] != '1' && f[i] != 'U' && f[i] != 'g' && f[i] != 'p')
			exit_usage(f[i]);
		i++;
	}
}
