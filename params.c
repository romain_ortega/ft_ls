/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   params.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 18:15:16 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:18:15 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	parse_path(char *path, t_data *p)
{
	int 	ret_open		= 0;
	DIR 	*ret_opendir	=NULL;
	
	if (!path)
		return ;
	ret_open = open(path, O_RDONLY);
	ret_opendir = opendir(path);
	if (ret_open == -1 && ret_opendir == NULL
		&& errno == ENOENT && errno != ENOTDIR)
	{
		p->d.path_set = true;
		save_error(&p->e, path);
	}
	else
	{
		if (ret_open != -1)
			close(ret_open);
		if (ret_opendir != NULL)
			closedir(ret_opendir);
		if (p->d.path == NULL)
			p->d.path = lst_new(path, ft_strlen(path));
		else
			lst_push_data(&p->d.path, path, ft_strlen(path));
		p->d.nb_path += 1;
		p->d.path_set = true;
	}
}

t_data		route(t_data p)
{
	if (!p.d.path_set)
		p.d.path = lst_new(".", 1);
	if (p.o.f_big_r)
		list_all_sub_dir(&p);
	if (p.d.path_set == true && !p.o.f_f)
		p.d.path = lst_sort(p.d.path, lst_cmp);
	if (p.e.error_set)
		sort_error(&p.e, p.o);
	return (p);
}

t_data		parse_args(int ac, char **av)
{
	t_data p;

	init_data(&p);
	INIT(int, ret, 1);
	INIT(int, i, 1);
	while (i < ac)
	{
		if (*av[i] == '-' && ret && ft_strcmp(av[i], "-") != 0)
		{
			if (ft_strcmp(av[i], "--") == 0)
				ret = 0;
			else
				parse_flag(av[i] + 1, &p);
		}
		else
		{
			ret = 0;
			if (av[i][0] == '\0')
				exit_error("ft_ls: fts_open: No such file or directory\n");
			parse_path(av[i], &p);
		}
		i++;
	}
	return (route(p));
}
