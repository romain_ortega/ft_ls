/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 16:50:03 by rortega           #+#    #+#             */
/*   Updated: 2016/01/14 16:50:30 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"
#include "ft_printf.h"

int	main(int ac, char **av)
{
	t_data p;

	p = parse_args(ac, av);
	list_dir(p);
	if (p.e.error_set == true)
		exit(1);
	return (0);
}
