/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_error.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <rortega@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 19:33:48 by rortega           #+#    #+#             */
/*   Updated: 2016/04/17 16:08:21 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "ft_printf.h"
#include "libft.h"

void			display_top_errors(t_error e)
{
	size_t	 	i = 0;

	while (i < e.nb_error)
	{
		if (e.display_top[i] == true)
		{
			if (e.file[i])
			{
				if (ft_strchr(e.file[i], '/') != NULL)
					e.file[i] = ft_strchr(e.file[i], '/') + 1;
				write(2, "ft_ls: ", 7);
				write(2, e.file[i], ft_strlen(e.file[i]));
				write(2, ": ", 2);
				write(2, e.error[i], ft_strlen(e.error[i]));
				ft_putchar('\n');
			}
			else
			{
				write(2, "ft_ls: ", 7);
				write(2, e.error[i], ft_strlen(e.error[i]));
				ft_putchar('\n');
				free_error(e, i);
			}
		}
		i++;
	}
}

static void		add_file(t_data *p, char *path)
{
	struct dirent 	file;
	struct stat 	*file_infos = NULL;

	if (!path)
		return ;
	ft_strcpy(file.d_name, path);
	file.d_type = DT_REG;
	p->d.dir_content = NULL;
	if (!(file_infos = (struct stat *)malloc(sizeof(struct stat))))
		exit_error("error: malloc fail");
	if ((lstat((const char *)path, file_infos)) < 0)
		dup_error(p, path);
	p->d.dir_content = lst_new_file(&file, file_infos);
	free(file_infos);
}

void			dup_error(t_data *p, char *path)
{
	struct dirent file;

	p->d.nb_dir_elem = 1;
	if (errno == ENOTDIR || !path)
		return (add_file(p, path));
	file.d_name[0] = '\0';
	file.d_type = DT_REG;
	p->d.dir_content = NULL;
	p->e.error_set = true;
	path = (!ft_strrchr(path, '/') ? path : ft_strrchr(path, '/') + 1);
	p->d.dir_content = lst_new_file(&file, NULL);
	p->d.dir_content->error = ft_strnew(12 + ft_strlen(path) +
	ft_strlen(strerror(errno)));
	ft_strcat(p->d.dir_content->error, "ft_ls: ");
	ft_strcat(p->d.dir_content->error, path);
	ft_strcat(p->d.dir_content->error, ": ");
	ft_strcat(p->d.dir_content->error, strerror(errno));
	ft_strcat(p->d.dir_content->error, "\n");
}

void			save_error(t_error *e, char *file)
{
	e->error_set = true;
	e->error[e->nb_error] = ft_strdup(strerror(errno));
	e->file[e->nb_error] = ft_strdup(file);
	e->nb_error += 1;
}

void			sort_error(t_error *e, t_opt o)
{
	size_t 		i;

	i = 0;
	if (!(e->display_top = (bool *)malloc(e->nb_error * sizeof(bool))))
		exit_error("error: malloc fail");
	if (o.f_f == false)
		quicksort_str((char const **)e->file, e->nb_error, ft_strcmp);
	while (i < e->nb_error)
	{
		if (ft_strstr(e->error[i], "No such file or directory"))
			e->display_top[i] = true;
		i++;
	}
}
