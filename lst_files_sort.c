/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_files_sort.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:05:32 by rortega           #+#    #+#             */
/*   Updated: 2016/02/01 18:05:33 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"

static t_file_list	*sortedmerge(t_file_list *a, t_file_list *b, char s,
					int (*cmp)(t_file_list *, t_file_list *, char s))
{
	t_file_list *result;

	result = NULL;
	if (a == NULL)
		return (b);
	else if (b == NULL)
		return (a);
	if (cmp(a, b, s) <= 0)
	{
		result = a;
		result->next = sortedmerge(a->next, b, s, cmp);
	}
	else
	{
		result = b;
		result->next = sortedmerge(a, b->next, s, cmp);
	}
	return (result);
}

static void			frontbacksplit(t_file_list *source, t_file_list **front_ref,
					t_file_list **back_ref)
{
	t_file_list	*fast;
	t_file_list	*slow;

	if (source == NULL || source->next == NULL)
	{
		*front_ref = source;
		*back_ref = NULL;
	}
	else
	{
		slow = source;
		fast = source->next;
		while (fast != NULL)
		{
			fast = fast->next;
			if (fast != NULL)
			{
				slow = slow->next;
				fast = fast->next;
			}
		}
		*front_ref = source;
		*back_ref = slow->next;
		slow->next = NULL;
	}
}

void				lst_file_sort(t_file_list **list, char s,
					int (*cmp)(t_file_list *, t_file_list *, char s))
{
	t_file_list *head;
	t_file_list *a;
	t_file_list *b;

	head = *list;
	if ((head == NULL) || (head->next == NULL))
		return ;
	frontbacksplit(head, &a, &b);
	lst_file_sort(&a, s, cmp);
	lst_file_sort(&b, s, cmp);
	*list = sortedmerge(a, b, s, cmp);
}
