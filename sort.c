/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rortega <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 18:16:22 by rortega           #+#    #+#             */
/*   Updated: 2016/01/22 18:16:23 by rortega          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft.h"

int			lst_file_cmp(t_file_list *elem1, t_file_list *elem2, char s)
{
	if (s == 'a')
	{
		if (!elem1->error)
			return (ft_strcmp(elem1->file_name, elem2->file_name));
		if (elem1->error && elem1->error)
			return (ft_strcmp(elem1->error, elem2->error));
	}
	else if (s == 'S')
		return (elem2->file_infos->st_size - elem1->file_infos->st_size);
	else if (s == 'c')
		return (elem2->file_infos->st_ctime - elem1->file_infos->st_ctime);
	else if (s == 't')
		return (elem2->file_infos->st_mtime - elem1->file_infos->st_mtime);
	else if (s == 'u')
		return (elem2->file_infos->st_atime - elem1->file_infos->st_atime);
	return (0);
}

int			lst_cmp(t_list *elem1, t_list *elem2)
{
	return (ft_strcmp(elem1->content, elem2->content));
}

void		sort_dir_content(t_data *p)
{
	char		s;

	s = 'a';
	if (p->o.f_f)
		return ;
	if (p->o.f_t)
		s = 't';
	else if (p->o.f_u)
		s = 'u';
	else if (p->o.f_c)
		s = 'c';
	else if (p->o.f_big_s)
		s = 'S';
	if (s == 'a')
	{
		lst_file_sort(&p->d.dir_content, s, lst_file_cmp);
		return ;
	}
	else
		lst_file_sort(&p->d.dir_content, 'a', lst_file_cmp);
	lst_file_sort(&p->d.dir_content, s, lst_file_cmp);
}
